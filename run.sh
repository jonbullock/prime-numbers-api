#!/bin/bash
# Runs tests and builds JAR
echo "====== Building project using Gradle wrapper ======"
./gradlew clean build
# Builds Docker image file
echo "====== Building Docker image ======"
docker build . -t prime-numbers-app
# Runs Docker container in detached mode
echo "====== Starting Docker container (called: prime-numbers) in detached mode using image ======"
docker run -d -p 8080:8080 --name prime-numbers prime-numbers-app:latest
# Wait 5secs for Docker container to start before opening browser
echo "====== Waiting 5 seconds for Docker container to start before opening local browser ======"
sleep 5s
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    # Opens local browser on Linux
    xdg-open http://localhost:8080
elif [[ "$OSTYPE" == "darwin"* ]]; then
    # Opens local browser on macOS
    open http://localhost:8080
else
        # Unknown.
        echo "====== Unable to detect local OS, please open local browser manually and goto: http://localhost:8080/ ======"
fi
echo "====== Remember to stop Docker container before running me again ======"

package com.jonathanbullock.primenumbers.api;

import org.apache.commons.math3.primes.Primes;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 *  Class for generating Prime Numbers.
 *  Utilises Apache Commons Math under the hood, see {@link Primes}.
 */
public class PrimeNumbersGenerator {
    /**
     * Gets all Prime numbers up to a supplied upper value.
     *
     * @param upperValue generate Prime numbers up to (and including) this value
     * @return Stream of Prime Numbers up to supplied upperValue
     */
    public Stream<Integer> getPrimeNumbersLessThan(Integer upperValue) {
        return IntStream.rangeClosed(1, upperValue).filter(Primes::isPrime).boxed();
    }
}

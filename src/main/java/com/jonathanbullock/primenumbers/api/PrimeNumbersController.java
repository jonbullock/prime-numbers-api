package com.jonathanbullock.primenumbers.api;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 *  Controller for generating Prime Numbers
 */
@RestController
public class PrimeNumbersController {

    /**
     * Returns a list of Prime numbers up to and including number supplied
     *
     * @param upperValue generate Prime numbers up to (and including) this value
     * @return wrapper POJO containing list of Prime numbers {@link Payload}
     */
    @GetMapping("/primes")
    @CrossOrigin(origins = "http://localhost:4200") // enables running UI in separate server process
    public Payload getPrimeNumbers(@RequestParam(value="upperValue", defaultValue = "50") int upperValue) {
        if (upperValue < 2) {
            return new Payload(Collections.emptyList());
        }
        return new Payload(
            new PrimeNumbersGenerator().getPrimeNumbersLessThan(upperValue)
                .collect(Collectors.toList())
        );
    }

    /**
     * Returns a list of Prime numbers up to and including number supplied that supports pagination.
     *
     * @param upperValue generate Prime numbers up to (and including) this value
     * @param offset index to start from in list of Prime numbers returned
     * @param pageSize number of Prime numbers to return from offset
     * @return wrapper POJO containing list of Prime numbers {@link Payload}
     */
    @GetMapping(value = "/primes",params = {"upperValue", "offset", "pageSize"})
    @CrossOrigin(origins = "http://localhost:4200") // enables running UI in separate server process
    public PayloadPaged getPrimeNumbersPaged(
        @RequestParam(value = "upperValue", defaultValue = "50") int upperValue,
        @RequestParam(value = "offset", defaultValue = "0") long offset,
        @RequestParam(value = "pageSize", defaultValue = "0") long pageSize) {
        PrimeNumbersGenerator numbersGenerator = new PrimeNumbersGenerator();
        List<Integer> numbers = numbersGenerator.getPrimeNumbersLessThan(upperValue).collect(Collectors.toList());
        if (offset == 0 && pageSize == 0) {
            // return all generated numbers
            return new PayloadPaged(numbers.size(), numbers);
        }
        return new PayloadPaged(
            numbers.size(),
            numbersGenerator.getPrimeNumbersLessThan(upperValue)
                .skip(offset)
                .limit(pageSize)
                .collect(Collectors.toList())
        );
    }
}

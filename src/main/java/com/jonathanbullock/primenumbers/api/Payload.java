package com.jonathanbullock.primenumbers.api;

import java.util.List;

/**
 * Wrapper POJO to aid serialisation, holds a list of Prime numbers
 *
 */
public class Payload {
    private List<Integer> primeNumbers;

    public List<Integer> getPrimeNumbers() {
        return primeNumbers;
    }

    public void setPrimeNumbers(List<Integer> primeNumbers) {
        this.primeNumbers = primeNumbers;
    }

    public Payload() {
    }

    public Payload(List<Integer> primeNumbers) {
        this.primeNumbers = primeNumbers;
    }
}

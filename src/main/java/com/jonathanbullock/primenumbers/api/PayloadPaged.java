package com.jonathanbullock.primenumbers.api;

import java.util.List;

/**
 * Wrapper POJO to aid serialisation, holds a list of Prime numbers and total count of numbers generated
 *
 */
public class PayloadPaged extends Payload {
    private Integer total;

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public PayloadPaged(Integer total, List<Integer> primeNumbers) {
        super(primeNumbers);
        this.total = total;
    }
}

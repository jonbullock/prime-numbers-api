package com.jonathanbullock.primenumbers.api;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;

import static org.assertj.core.api.Assertions.*;

/**
 * Integration tests for API, tests calls and responses.
 *
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PrimeNumbersAPITests {

    @Test
    void contextLoads(@Autowired PrimeNumbersController primeController) {
        assertThat(primeController).isNotNull();
    }

    @Test
    public void primesDefaultTest(@Autowired TestRestTemplate restTemplate) {
        // given
        // endpoint should default to value of 50 if no value specified for 'upperValue' param
        // when
        Payload payload = restTemplate.getForObject("/primes", Payload.class);
        // then
        assertThat(payload.getPrimeNumbers())
                .hasSize(15)
                .contains(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47);
    }

    @Test
    public void primesEmptyTest(@Autowired TestRestTemplate restTemplate) {
        // given
        // endpoint should return empty list when 'upperValue' is < 2
        // when
        Payload payload = restTemplate.getForObject("/primes?upperValue=1", Payload.class);
        // then
        assertThat(payload.getPrimeNumbers())
            .isEmpty();
    }

    @Test
    public void primesUpTo5Test(@Autowired TestRestTemplate restTemplate) {
        // given
        // generate Prime numbers up to the number 5
        // when
        Payload payload = restTemplate.getForObject("/primes?upperValue=5", Payload.class);
        // then
        assertThat(payload.getPrimeNumbers())
                .hasSize(3)
                .contains(2, 3, 5);
    }

    @Test
    public void primesUpTo50PagedBy5Test(@Autowired TestRestTemplate restTemplate) {
        // given
        // generate Prime numbers up to the number 50 but only return 5 numbers from the 5th number
        // when
        PayloadPaged payload = restTemplate.getForObject("/primes?upperValue=50&offset=5&pageSize=5", PayloadPaged.class);
        // then
        assertThat(payload.getTotal())
            .isEqualTo(15);
        assertThat(payload.getPrimeNumbers())
            .hasSize(5)
            .contains(13, 17, 19, 23, 29);
    }

    @Test
    public void primesUpTo50WithPagingParamsSetToZeroTest(@Autowired TestRestTemplate restTemplate) {
        // given
        // generate Prime numbers up to the number 50 with 0 set for paging params, should return all Primes
        // when
        PayloadPaged payload = restTemplate.getForObject("/primes?upperValue=50&offset=0&pageSize=0", PayloadPaged.class);
        // then
        assertThat(payload.getTotal())
            .isEqualTo(15);
        assertThat(payload.getPrimeNumbers())
            .hasSize(15)
            .contains(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47);
    }
}

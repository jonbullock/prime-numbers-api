package com.jonathanbullock.primenumbers.api;

import org.junit.jupiter.api.Test;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.*;

/**
 * Unit tests for {@link PrimeNumbersGenerator} class.
 *
 */
public class PrimeNumbersGeneratorUnitTests {

    /**
     * Test Spec: Input: 5 Result: 2, 3, 5
     */
    @Test
    public void primeNumbersUpTo5Test() {
        // given
        PrimeNumbersGenerator primeGenerator = new PrimeNumbersGenerator();
        // when
        Stream<Integer> result = primeGenerator.getPrimeNumbersLessThan(5);
        // then
        assertThat(result)
                .hasSize(3)
                .contains(2, 3, 5);
    }

    /**
     * Test Spec: Input: 50 Result: 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47.
     */
    @Test
    public void primeNumbersUpTo50Test() {
        // given
        PrimeNumbersGenerator primeGenerator = new PrimeNumbersGenerator();
        // when
        Stream<Integer> result = primeGenerator.getPrimeNumbersLessThan(50);
        // then
        assertThat(result)
                .hasSize(15)
                .contains(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47);
    }
}
